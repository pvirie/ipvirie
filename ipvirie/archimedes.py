
import numpy as np
import time
import json
import http.client
import random


def build_package(l, r):
    return "l=" + str(l) + "&r=" + str(r)


class ArchConnector:

    def __init__(self, sim_id, host="localhost", port=9999, secured=True):
        self.sim_id = sim_id
        if secured:
            self.conn = http.client.HTTPSConnection(host, port)
        else:
            self.conn = http.client.HTTPConnection(host, port)

    def do(self, l, r):
        self.conn.request("GET", "/command?target_id=" + self.sim_id + "&" + build_package(l, r))
        r1 = self.conn.getresponse()
        data = r1.read()
        dict_data = json.loads(data)
        del dict_data["id"]
        del dict_data["type"]
        return dict_data


if __name__ == '__main__':

    connector = ArchConnector("*", host="exp.pvirie.com", port=9999, secured=True)
    for i in range(1000):
        print(connector.do(random.random(), random.random()))
