import numpy as np
import os
import cv2
import math


def get_dataset(color=True, half_scale=False):
    icon_path = os.path.join(os.getcwd(), "data", "icons")

    all_images = []
    for f in os.listdir(icon_path):
        img = cv2.imread(os.path.join(icon_path, f))
        if not color:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        if half_scale:
            img = cv2.resize(img, (16, 16))

        all_images.append(img / 255.0)

    np_images = np.stack(all_images, axis=0)
    return np_images


def make_samples(images, items_per_row=8):
    rows = math.ceil(images.shape[0] / items_per_row)
    cols = items_per_row
    height = images.shape[1]
    width = images.shape[2]
    if len(images.shape) == 3:
        canvas = np.zeros((height * rows, width * cols), dtype=np.float32)
        for i in range(images.shape[0]):
            x = i % items_per_row
            y = i // items_per_row
            canvas[(height * y): (height * (y + 1)), (width * x): (width * (x + 1))] = np.reshape(images[i, ...], [height, width])
    else:
        canvas = np.zeros((height * rows, width * cols, images.shape[3]), dtype=np.float32)
        for i in range(images.shape[0]):
            x = i % items_per_row
            y = i // items_per_row
            canvas[(height * y): (height * (y + 1)), (width * x): (width * (x + 1)), ...] = np.reshape(images[i, ...], [height, width, -1])
    return canvas
