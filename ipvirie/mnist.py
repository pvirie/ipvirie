import numpy as np
import struct
import os


def read_idx(filename):
    with open(filename, 'rb') as f:
        zero, data_type, dims = struct.unpack('>HBB', f.read(4))
        shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
        return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)


def get_dataset(sample_per_class=100, test_per_class=10):
    mnist_test_image = os.path.join(os.getcwd(), "data", "t10k-images-idx3-ubyte")
    mnist_test_label = os.path.join(os.getcwd(), "data", "t10k-labels-idx1-ubyte")
    test_images = read_idx(mnist_test_image)
    test_labels = read_idx(mnist_test_label)

    selected_indices = []
    test_indices = []
    count_class = np.zeros(10, dtype=np.int32)
    for i in range(test_labels.shape[0]):
        label = test_labels[i]
        if count_class[label] < sample_per_class:
            selected_indices.append(i)
        elif count_class[label] < sample_per_class + test_per_class:
            test_indices.append(i)
        count_class[label] = count_class[label] + 1

    selected_labels = test_labels[selected_indices]
    onehot_labels = np.zeros((selected_labels.shape[0], 10), dtype=np.float32)
    onehot_labels[np.arange(selected_labels.shape[0]), selected_labels] = 1.0
    return test_images[selected_indices, ...].astype(np.float32) / 255.0, onehot_labels, test_images[test_indices, ...].astype(np.float32) / 255.0


def sort_many(subset_images, labels):
    classes = np.argmax(labels, axis=1)
    bin = np.bincount(classes)
    rows = np.max(bin)
    cols = 10
    canvas = np.zeros((28 * rows, 28 * cols), dtype=np.float32)
    for i in range(10):
        mask = (classes == i)
        ranks = np.argsort(labels[mask, i] * -1)
        selected = np.nonzero(mask)[0][ranks]
        canvas[0:(28 * selected.shape[0]), (28 * i): (28 * (i + 1))] = np.reshape(subset_images[selected, ...], [-1, 28])
    return canvas


if __name__ == '__main__':
    import os
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(dir_path, "..", "data")

    mnist_test_image = os.path.join(data_dir, "t10k-images-idx3-ubyte")
    mnist_test_label = os.path.join(data_dir, "t10k-labels-idx1-ubyte")

    test_images = read_idx(mnist_test_image)
    test_labels = read_idx(mnist_test_label)
    print(test_images.shape, test_images.dtype)
    print(test_labels.shape, test_labels.dtype)

    onehot_labels = np.zeros((test_labels.shape[0], 10), dtype=np.float32)
    onehot_labels[np.arange(test_labels.shape[0]), test_labels] = 1.0

    canvas = sort_many(test_images[100:200, ...].astype(np.float32) / 255.0, onehot_labels[100:200])

    import cv2
    cv2.imshow("test", canvas)
    cv2.waitKey(-1)
