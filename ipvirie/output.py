import PIL.Image
from IPython.display import *
import numpy as np
import librosa.display
import matplotlib.pyplot as plt

import ipvirie.core


def imshow(im, clear=False):
    im = np.uint8(im)
    display(PIL.Image.fromarray(im))
    if clear:
        clear_output(wait=True)


def wvshow(w, sampling_rate=22050):
    plt.figure(figsize=(14, 5))
    librosa.display.waveplot(w, sr=sampling_rate)


def wvplay(w, sampling_rate=22050):
    Audio(w, rate=sampling_rate)
