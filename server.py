import tornado.ioloop
import tornado.web
import tornado.websocket
import os
import webbrowser
import json
import asyncio
import time
import subprocess


file_path = os.path.dirname(os.path.abspath(__file__))
static_path = os.path.join(file_path, "./web")


sim_connections = {}
futures = {}


class CommandHandler(tornado.web.RequestHandler):
    async def get(self):

        target_id = self.get_argument('target_id', None)
        left_str = self.get_argument('l', None)
        right_str = self.get_argument('r', None)

        msg_id = str(time.time())
        futures[msg_id] = asyncio.Future()

        if target_id == "*":
            for conn in sim_connections.values():
                conn.write_message({"id": msg_id, "l": float(left_str), "r": float(right_str)})
            message = await futures[msg_id]
            self.write(message)
        else:

            conn = sim_connections.get(target_id)
            if conn is not None:
                conn.write_message({"id": msg_id, "l": float(left_str), "r": float(right_str)})
                message = await futures[msg_id]
                self.write(message)
            else:
                self.write("{}")


class SimpleWebSocket(tornado.websocket.WebSocketHandler):

    def open(self):
        print("Connection opened.")

    def on_message(self, message):
        obj = json.loads(message)
        if obj["type"] == "reg":
            self.id = obj["id"]
            sim_connections[self.id] = self
            print(self.id, "has connected", len(sim_connections))
        else:
            promise = futures.get(obj["id"])
            if promise is not None:
                promise.set_result(json.dumps(obj))
                del futures[obj["id"]]

    def on_close(self):
        del sim_connections[self.id]
        print(self.id, "has left", len(sim_connections))


class UpdateHandler(tornado.web.RequestHandler):
    def get(self):
        subprocess.run(["pip", "install", "--upgrade", "git+https://bitbucket.org/pvirie/ipvirie.git#egg=ipvirie"])
        self.write("Done!")


class UpdateWebHandler(tornado.web.RequestHandler):
    def get(self):
        subprocess.run(["git", "pull", "origin", "master"], cwd=file_path)
        self.write("Done!")


def make_app():
    return tornado.web.Application([
        (r"/command", CommandHandler),
        (r"/websocket", SimpleWebSocket),
        (r"/update", UpdateHandler),
        (r"/updateweb", UpdateWebHandler),
        (r'/(.*)', tornado.web.StaticFileHandler, {'path': static_path})
    ])


if __name__ == "__main__":

    app = make_app()
    http_server = tornado.httpserver.HTTPServer(app)
    # http_server = tornado.httpserver.HTTPServer(app, ssl_options={
    #     "certfile": "/etc/letsencrypt/live/exp.pvirie.com/fullchain.pem",
    #     "keyfile": "/etc/letsencrypt/live/exp.pvirie.com/privkey.pem",
    # })
    http_server.listen(9999)

    webbrowser.open("http://localhost:9999/index.html")
    tornado.ioloop.IOLoop.instance().start()
