try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

long_description = open('README.md').read()
packages = ['ipvirie']

setup(
    name="IPVirie",
    version="0.1dev",
    description='IPython Virie\'s personal packages',
    long_description=long_description,
    keywords='IPython Input Output Algorithms',
    author='Patrick Virie',
    maintainer='Patrick Virie',
    url='https://bitbucket.org/pvirie/ipvirie',
    license="MIT License",
    packages=packages,
    install_requires=[
        'numpy',
        'pillow',
        'librosa',
        'matplotlib'
    ]
)
