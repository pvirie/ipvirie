function Archimedes(area_unit) {

	let Tau = Math.PI*2;

	function Mob(pos, size) {
		this.x = pos[0];
		this.y = pos[1];
		this.s = size; //radius
	}


	function Robot(pos, size, orientation) {
		this.x = pos[0];
		this.y = pos[1];
		this.o = orientation;
		this.s = size; //radius
		this.has_iis = true;

		let noise_scale = 0.001;
		let handicap = 0.7;
		this.move = function(l_, r_) {
			r_ = r_*handicap;
			let l = l_ + Math.abs(l_)*(Math.random() - 0.5)*noise_scale;
			let r = r_ + Math.abs(r_)*(Math.random() - 0.5)*noise_scale;

			let cos = Math.cos(this.o);
			let sin = -Math.sin(this.o);

			let omega = (r - l)/(2*this.s);

			let pd = l*(2*this.s)/(l - r + 1e-8); //distance from the left wheel	

			//compute new center
			let bar = pd - this.s;

			let px = this.x - sin*bar;
			let py = this.y + cos*bar;

			hres = check_hit(this, omega, [px, py]);
			mres = check_mob(this);
			let carry = false;
			if(mres[1][0] <= (this.s + mob.s) && !check_base(this)) {
				carry = true;
			}

			this.o += hres.do;
			let cos_ = Math.cos(this.o);
			let sin_ = -Math.sin(this.o);

			this.x = px + sin_*bar;
			this.y = py - cos_*bar;

			if(carry) {
				carry_mob(this);
			}

			let mratio = hres.do/(omega + 1e-8);

			return [l_*mratio, r_*mratio, this.o, hres.ps? hres.ps - this.o:null, this.has_iis? mres[0]: null];
		}
	}

	function Arena(center, a, b, orientation) {
		this.cx = center[0];
		this.cy = center[1];
		this.o = orientation;
		let cos = Math.cos(orientation);
		let sin = -Math.sin(orientation);
		this.ax = [cos, sin];
		this.ay = [sin, -cos];
		this.a = a;
		this.b = b;

		this.normals = [
			[-cos, -sin],
			[-sin, cos],
			this.ax,
			this.ay
		];

		this.bases = [
			[this.ax[0]*a + this.cx, this.ax[1]*a + this.cy],
			[this.ay[0]*b + this.cx, this.ay[1]*b + this.cy],
			[- this.ax[0]*a + this.cx, - this.ax[1]*a + this.cy],
			[- this.ay[0]*b + this.cx, - this.ay[1]*b + this.cy]
		];

		this.is_in = function(x, y, s) {
			let dx = x - this.cx;
			let dy = y - this.cy;
			let dotX = dx*this.ax[0] + dy*this.ax[1];
			let dotY = dx*this.ay[0] + dy*this.ay[1];

			return (Math.abs(dotX) < this.a - s && Math.abs(dotY) < this.b - s)
		}

		this.get_corners = function() {
			return [
				[this.ax[0]*a + this.ay[0]*b + this.cx, this.ax[1]*a + this.ay[1]*b + this.cy],
				[this.ax[0]*a - this.ay[0]*b + this.cx, this.ax[1]*a - this.ay[1]*b + this.cy],
				[- this.ax[0]*a - this.ay[0]*b + this.cx, - this.ax[1]*a - this.ay[1]*b + this.cy],
				[- this.ax[0]*a + this.ay[0]*b + this.cx, - this.ax[1]*a + this.ay[1]*b + this.cy]
			];
		}
	}

	let sqr_distance = function(x0, y0, x1, y1) {
		return (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1);
	}

	let dot_p = function(v, u) {
		return v[0]*u[0] + v[1]*u[1];
	}

	let cross_p = function(v, u) {
		return v[0]*u[1] - v[1]*u[0];
	}

	let check_hit_line = function(l, p, b, n, s) {
		let k = s - dot_p(p, n) + dot_p(b, n);
		let cc = l[0]*n[0] + l[1]*n[1];
		let cs = l[1]*n[0] - l[0]*n[1];

		let A = (cs*cs + cc*cc);
		let B = -2*cs*k / A;
		let C = (k*k - cc*cc) / A;

		let inroot = B*B - 4*C;

		if(inroot < 0)
			return null;

		let root = Math.sqrt(inroot);
		let s0 = ((-B + root) / (2 + 1e-8));
		let c0 = Math.sqrt(1-s0*s0);
		let s1 = ((-B - root) / (2 + 1e-8));
		let c1 = Math.sqrt(1-s1*s1);

		let at0 = Math.atan2(s0, c0);
		let at1 = Math.atan2(s1, c1);

		return [at0, at1];
	}

	let mod_theta_base = function(t, neg=false) {
		if(neg) return (-Tau + t) % Tau;
		return (Tau + t) % Tau;
	}

	var robot = null;
	var arena = null;
	var mob = null;

	let check_base = function(robot) {
		let l = [robot.x - arena.cx, robot.y - arena.cy];
		let sqr_cen = dot_p(l, l);

		if((arena.a+arena.b)*(arena.a+arena.b)*0.1*0.1 > sqr_cen) {
			return true;
		}
		return false;
	}

	let check_hit = function(robot, omega, pivot) {
		let res = {};

		let max_disp = robot.s + Math.abs(omega)*Math.sqrt(sqr_distance(pivot[0], pivot[1], robot.x, robot.y));
		if(arena.is_in(robot.x, robot.y, max_disp)) {
			res.do = omega;
			res.ps = null;
			return res; 
		}
		let l = [robot.x - pivot[0], robot.y - pivot[1]];
		
		let windex = null;
		let best = mod_theta_base(omega, omega < 0);
		for(var i = 0;i<4;++i){

			let thetas = check_hit_line(l, pivot, arena.bases[i], arena.normals[i], robot.s);
			if(thetas == null) continue;
			if(omega < 0) {
				let t = Math.max(mod_theta_base(thetas[0], true), mod_theta_base(thetas[1], true));
				if(best == null || best < t) {
					best = t;
					windex = i;
				}
			}else{ 
				let t = Math.min(mod_theta_base(thetas[0]), mod_theta_base(thetas[1]));
				if(best == null || best > t){
					best = t;
					windex = i;
				}
			}
		}

		res.do = (omega < 0? best + 1e-8 : best - 1e-8);
		if(windex == null) res.ps = null;
		else res.ps = arena.o + windex*Math.PI/2;
		return res;
	}

	let confusticate = function(dist, dtheta) {
		return [dtheta/(dist+1), dist*dtheta*dtheta];
	}

	let check_mob = function(robot){
		let dx = robot.x - mob.x;
		let dy = robot.y - mob.y;

		let dist = Math.sqrt(dx*dx + dy*dy);
		let dtheta = Math.PI - Math.atan2(dy, dx) - robot.o;

        while (dtheta > Math.PI) dtheta -= Math.PI * 2;
        while (dtheta < -Math.PI) dtheta += Math.PI * 2;

		// console.log(dist, dtheta*180/Math.PI);
		return [confusticate(dist, dtheta), [dist, dtheta]];
	}
	
	let check_push_line = function(base, normal) {
		let bx = base[0] + normal[0]*mob.s;
		let by = base[1] + normal[1]*mob.s;
		let dx = mob.x - bx;
		let dy = mob.y - by;

		let c = dx*normal[1] - dy*normal[0];
		let d = dx*normal[0] + dy*normal[1];

		if(d < 0) {
			mob.x = bx + normal[1]*c - normal[0]*d;
			mob.y = by - normal[0]*c - normal[1]*d;
			return true;
		}else{
			return false;
		}
	}

	this.push_mob = function(vx, vy) {
		let max_disp = mob.s + Math.sqrt(vx*vx + vy*vy);
		if(arena.is_in(mob.x, mob.y, max_disp)) {

			mob.x = mob.x + vx;
			mob.y = mob.y + vy;
			return [vx, vy];
		}else{

			mob.x = mob.x + vx;
			mob.y = mob.y + vy;
			for(var i = 0;i<4;++i){
				let b = arena.bases[i];
				let n = arena.normals[i];
				if(check_push_line(b, n)) {
					let d = 2*(n[0]*vx + n[1]*vy);
					return [vx - d*n[0], vy - d*n[1]];
				}
			}

			return [vx, vy];
		}
	}

	this.drag_mob = function(x, y) {
		mob.x = x;
		mob.y = y;
		return check_mob(robot);
	}

	let carry_mob = function(robot) {
		mob.x = robot.x;
		mob.y = robot.y;
	}

	this.reset_world = function() {
		delete robot;
		delete arena;
		delete mob;

		robot = new Robot(
			[randInt(-area_unit/2, area_unit/2), randInt(-area_unit/2, area_unit/2)], 
			area_unit/15,
			Math.random()*Tau);

		arena = new Arena(
			[0, 0], 
			randInt(area_unit*Math.sqrt(2)/2 + area_unit/15, area_unit), randInt(area_unit*Math.sqrt(2)/2 + area_unit/15, area_unit), 
			Math.random()*Tau);

		mob = new Mob(
			[randInt(-area_unit/2, area_unit/2), randInt(-area_unit/2, area_unit/2)], 
			area_unit/30
			)

		return [robot, arena, mob];
	}

	
	let randInt = function(min, max) {
		return Math.floor(Math.random() * (max - min) ) + min;
	}

};