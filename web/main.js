paper.install(window);
window.onload = function() {
    paper.setup('main_canvas');
    let main = new Main();
}

//https://techoverflow.net/2018/03/30/copying-strings-to-the-clipboard-using-pure-javascript/
function copyStringToClipboard(str) {
    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = { position: 'absolute', left: '-9999px' };
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
}

function Main() {

    var session_id = String(new Date().getTime());
    window.location.hash = session_id;
    var unit_distance = 500;
    var world = new Archimedes(unit_distance);

    let grid_start = new Point();
    let grid_on = function(point) {
        grid_start = point;
    }

    let grid = null;
    let grid_move = function(point) {
        let scale_x = point.y - grid_start.y;
        let scale_y = grid_start.x - point.x;

        let grid_lines = [new Path.Line({
                from: grid_start,
                to: point,
                strokeColor: 'yellow',
                strokeWidth: 1
            })];

        let scale_count = 5;
        let along_x = (point.x - grid_start.x) / (scale_count-1);
        let along_y = (point.y - grid_start.y) / (scale_count-1);
        for(let i = 0;i< scale_count;++i) {
            let line_width = 0.2 - i*0.02;
            grid_lines.push(new Path.Line({
                from: new Point(grid_start.x + along_x*i + scale_x*line_width, grid_start.y + along_y*i + scale_y*line_width),
                to: new Point(grid_start.x + along_x*i - scale_x*line_width, grid_start.y + along_y*i - scale_y*line_width),
                strokeColor: 'yellow',
                strokeWidth: 1
            }));
        }

        let new_grid = new Group(grid_lines);

        let temp = grid;
        grid = new_grid;
        if (temp != null) temp.remove();
    }

    let robot_walls = world.reset_world();
    let robot = robot_walls[0];
    let walls = robot_walls[1];
    let mob = robot_walls[2];

    let add_wall = function(p0, p1) {
        let new_edge = new Path();
        new_edge.sendToBack();
        new_edge.strokeColor = new Color(0.2, 0.1, 0.1);
        new_edge.strokeWidth = 10;
        new_edge.opacity = 1.0;
        new_edge.add(new Point(p0[0], p0[1]));
        new_edge.add(new Point(p1[0], p1[1]));
        return new_edge;
    }

    let corners = walls.get_corners();
    let wall0 = add_wall(corners[0], corners[1]);
    let wall1 = add_wall(corners[1], corners[2]);
    let wall2 = add_wall(corners[2], corners[3]);
    let wall3 = add_wall(corners[3], corners[0]);

    let base = new Group([
        new Path.Line({
            from: new Point((corners[0][0] - walls.cx) * 0.2 + walls.cx, (corners[0][1] - walls.cy) * 0.2 + walls.cy),
            to: new Point((corners[1][0] - walls.cx) * 0.2 + walls.cx, (corners[1][1] - walls.cy) * 0.2 + walls.cy),
            strokeColor: new Color(0.4, 0.4, 0.8),
            strokeWidth: 3,
            dashArray: [10, 12]
        }),
        new Path.Line({
            from: new Point((corners[1][0] - walls.cx) * 0.2 + walls.cx, (corners[1][1] - walls.cy) * 0.2 + walls.cy),
            to: new Point((corners[2][0] - walls.cx) * 0.2 + walls.cx, (corners[2][1] - walls.cy) * 0.2 + walls.cy),
            strokeColor: new Color(0.4, 0.4, 0.8),
            strokeWidth: 3,
            dashArray: [10, 12]
        }),
        new Path.Line({
            from: new Point((corners[2][0] - walls.cx) * 0.2 + walls.cx, (corners[2][1] - walls.cy) * 0.2 + walls.cy),
            to: new Point((corners[3][0] - walls.cx) * 0.2 + walls.cx, (corners[3][1] - walls.cy) * 0.2 + walls.cy),
            strokeColor: new Color(0.4, 0.4, 0.8),
            strokeWidth: 3,
            dashArray: [10, 12]
        }), new Path.Line({
            from: new Point((corners[3][0] - walls.cx) * 0.2 + walls.cx, (corners[3][1] - walls.cy) * 0.2 + walls.cy),
            to: new Point((corners[0][0] - walls.cx) * 0.2 + walls.cx, (corners[0][1] - walls.cy) * 0.2 + walls.cy),
            strokeColor: new Color(0.4, 0.4, 0.8),
            strokeWidth: 3,
            dashArray: [10, 12]
        })

    ]);

    let robot_graphic = new Group([
        new Path.Circle({
            center: new Point(robot.x, robot.y),
            radius: robot.s,
            fillColor: new Color(0.0, 0.8, 1.0),
            opacity: 0.8
        }),
        new Path.Line({
            from: new Point(robot.x, robot.y),
            to: new Point(robot.x + robot.s, robot.y),
            strokeColor: 'red',
            strokeWidth: 5
        })
    ]);

    robot_graphic.applyMatrix = false;
    robot_graphic.rotation = -robot.o * 180 / Math.PI;

    let mob_graphic = new Path.Circle({
        center: new Point(mob.x, mob.y),
        radius: mob.s,
        fillColor: new Color(1.0, 0.5, 0.5),
        opacity: 0.8
    });

    let hit_highlight = null;

    let mid_radian = function(r0, r1) {
        while (r1 > r0 + Math.PI) r1 -= Math.PI * 2;
        while (r1 < r0 - Math.PI) r1 += Math.PI * 2;
        return (r0 + r1) * 0.5;
    }

    let mob_highlight = null;
    let update_highlight = function() {

        let dir = -robot.o;
        let cos = Math.cos(dir);
        let sin = Math.sin(dir);
        let dx = robot.x - mob.x;
        let dy = robot.y - mob.y;
        let dist = Math.sqrt(dx * dx + dy * dy);

        let mob_o = Math.atan2(mob.y - robot.y, mob.x - robot.x);
        let ecos = Math.cos(mob_o);
        let esin = Math.sin(mob_o);

        let mid_o = mid_radian(mob_o, dir);
        let mcos = Math.cos(mid_o);
        let msin = Math.sin(mid_o);

        let new_highlight = new Group([
            new Path.Arc({
                from: new Point(robot.x + dist * cos * 0.5, robot.y + dist * sin * 0.5),
                through: new Point(robot.x + dist * mcos * 0.5, robot.y + dist * msin * 0.5),
                to: new Point(robot.x + dist * ecos * 0.5, robot.y + dist * esin * 0.5),
                strokeColor: 'yellow',
                strokeWidth: 2
            }),
            new Path.Line({
                from: new Point(robot.x + dist * ecos * 0.25, robot.y + dist * esin * 0.25),
                to: new Point(mob.x, mob.y),
                strokeColor: 'yellow',
                strokeWidth: 2
            }),
            new Path.Line({
                from: new Point(robot.x + dist * cos * 0.25, robot.y + dist * sin * 0.25),
                to: new Point(robot.x + dist * cos, robot.y + dist * sin),
                strokeColor: 'yellow',
                strokeWidth: 2
            })
        ]);

        let temp = mob_highlight;
        mob_highlight = new_highlight;
        if (temp != null) temp.remove();
    }

    let reset_highlight = function() {
        if (mob_highlight != null) mob_highlight.remove();
    }

    let mob_moving = false;
    let mob_wake_h = null;
    let mobsvx = null;
    let mobsvy = null;
    let mob_timer = function() {
        mob_moving = false;
        clearInterval(mob_wake_h);
        mob_wake_h = setTimeout(function() {
            mob_moving = true;
            mobsvx = (Math.random() - 0.5) * 10;
            mobsvy = (Math.random() - 0.5) * 10;
        }, 5000);
    }
    mob_timer();

    let measure = [];
    let last_point = new Point();
    let drag_mob = function(point) {
        let m = world.drag_mob(point.x, point.y);
        if(last_point.getDistance(point) > 100) 
        {
            measure.push(m);
            while(measure.length > 200) {
                measure.shift();
            }
            last_point = point;
        }
        mob_graphic.position = new Point(point.x, point.y);
        update_highlight();
        mob_timer();
    }

    let release_mob = function() {
        copyStringToClipboard(JSON.stringify(measure));
        reset_highlight();
    }

    let move_dist = 0;
    let mob_move = function() {

        let out = world.push_mob(mobsvx, mobsvy);
        mobsvx = out[0] * 0.99;
        mobsvy = out[1] * 0.99;
        let x = mob.x;
        let y = mob.y;

        let measure = world.drag_mob(x, y);
        mob_graphic.position = new Point(x, y);
        move_dist += 1;
        if (move_dist > 100) {
            mob_timer();
            move_dist = 0;
        }
    }

    window.action = function(l, r) {

        if (hit_highlight != null) hit_highlight.remove();

        let res = robot.move(l, r);
        robot_graphic.position = new Point(robot.x, robot.y);
        robot_graphic.applyMatrix = false;
        robot_graphic.rotation = -robot.o * 180 / Math.PI;

        mob_graphic.position = new Point(mob.x, mob.y);

        if (res[3] != null) {
            let abs_hit_theta = (-res[3] - robot.o);

            hit_highlight = new Path.Circle({
                center: new Point(robot.x + robot.s * Math.cos(abs_hit_theta), robot.y + robot.s * Math.sin(abs_hit_theta)),
                radius: 10,
                fillColor: new Color(1.0, 0.5, 0.5),
                opacity: 0.8
            });
        }

        // update_highlight();
        return res;
    }

    let protocol = window.location.protocol.localeCompare("https:") === 0? "wss://":"ws://";
    let address = protocol + window.location.hostname + ":9999/websocket";
    console.log("connecting to ", address);
    var ws = new WebSocket(address);
    ws.onmessage = function(event) {
        // console.log(event.data);

        let message = JSON.parse(event.data);
        let res = action(message.l, message.r);

        if (robot_walls != null) {
            let hit_pos = res[3];
            let bumps = null;

            while(hit_pos > Math.PI) hit_pos -= Math.PI*2;
            while(hit_pos < -Math.PI) hit_pos += Math.PI*2;
            if (hit_pos == null) bumps = [false, false, false, false];
            else bumps = [-Math.PI * 0.25 < hit_pos && hit_pos < Math.PI * 0.25,
                Math.PI * 0.25 < hit_pos && hit_pos < Math.PI * 0.75,
                Math.PI * 0.75 < hit_pos || hit_pos < -Math.PI * 0.75,
                -Math.PI * 0.75 < hit_pos && hit_pos < -Math.PI * 0.25
            ];

            ws.send(JSON.stringify({
                type: "res",
                id: message["id"],
                dl: res[0],
                dr: res[1],
                bumps: bumps,
                ifs: res[4]
            }));
        }

    };

    function waitForSocketConnection(socket, callback) {
        setTimeout(function() {
            if (socket.readyState === 1) {
                console.log("Connection is made")
                if (callback != null) {
                    callback();
                }
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 5); // wait 5 milisecond for the connection...
    }

    waitForSocketConnection(ws, function() {
        ws.send(JSON.stringify({ id: session_id, type: "reg" }));
    });

    ////////////////////////////////////////////////// VIEW //////////////////////////////////////////////////////////////

    var stop_all = false;
    var current_on_drag_handle = null;
    var current_on_click_handle = null;
    var current_on_down_handle = null;
    var current_on_up_handle = null;
    var tools = [];
    var icon_size = 0;
    var icon_gap = 0;
    (function() {

        let select_i = function(s) {
            for (let i = 0; i < tools.length; ++i) {
                tools[i].opacity = (i == s ? 1.0 : 0.2);
                tools[i].applyMatrix = false;
            }

            current_on_drag_handle = null;
            current_on_click_handle = null;
            current_on_up_handle = null;
            current_on_down_handle = null;
        }

        let create_menu_item = function(p, c) {
            let menu_item = new Path.Circle({
                center: p,
                radius: icon_size,
                fillColor: c,
                opacity: 1.0
            });
            menu_item.onMouseEnter = function(event) {
                stop_all = true;
            }
            menu_item.onMouseLeave = function(event) {
                stop_all = false;
            }
            tools.push(menu_item);
            return menu_item;
        }

        s = view.size;
        w = s.width;

        icon_size = w * 0.02;
        icon_gap = w * 0.05;

        create_menu_item(new Point(0, 0), new Color(0.4, 0.7, 1.0)).onClick = function(event) {
            select_i(0);
            this.tog = true;
            current_on_drag_handle = function(event) {
                if (this.tog) view.translate(event.delta);
                this.tog = !this.tog;
            }
            event.stop();
        }

        create_menu_item(new Point((icon_gap + icon_size * 2) * 1, 0), new Color(0.0, 1.0, 0.0)).onClick = function(event) {
            select_i(1);
            current_on_drag_handle = function(event) {
                let deltax = event.delta.x + event.delta.y;
                view.scale(1 + Math.sign(deltax) * 0.01);
            }
            event.stop();
        }


        create_menu_item(new Point((icon_gap + icon_size * 2) * 2, 0), new Color(1.0, 0.3, 0.3)).onClick = function(event) {
            select_i(2);
            current_on_drag_handle = function(event) {
                drag_mob(event.point);
            }
            current_on_up_handle = function(event) {
                release_mob();
            }
            event.stop();
        }

        create_menu_item(new Point((icon_gap + icon_size * 2) * 3, 0), new Color(1.0, 1.0, 0.0)).onClick = function(event) {
            select_i(3);
            current_on_down_handle = function(event) {
                grid_on(event.point);
            }
            current_on_drag_handle = function(event) {
                grid_move(event.point);
            }
            event.stop();
        }

    })();

    view.translate(new Point(view.size.width / 2, view.size.height / 2 / 2));

    view.onFrame = function(event) {
        s = view.size;
        w = s.width;
        h = s.height;
        c = view.center.subtract(new Point(w / 2, h / 2).subtract(new Point(icon_size + 10, icon_size + 10).divide(view.scaling)));

        for (let i = 0; i < tools.length; ++i) {
            tools[i].position = c.add(new Point((icon_gap + icon_size * 2) * i, 0).divide(view.scaling));
            tools[i].scaling = new Size(1.0 / view.scaling.x, 1.0 / view.scaling.y);
        }

        if (mob_moving) {
            mob_move();
        }
    }

    view.onMouseMove = function(event) {
        if(grid != null) {
            let dist = event.point.getDistance(grid_start);
            grid.opacity = Math.max(1.0 - dist*0.002, 0);
        }
    }

    view.onMouseDrag = function(event) {
        if (current_on_drag_handle != null && !stop_all)
            current_on_drag_handle(event);
    }

    view.onClick = function(event) {
        if (current_on_click_handle != null && !stop_all)
            current_on_click_handle(event);
    }

    view.onMouseUp = function(event) {
        if (current_on_up_handle != null && !stop_all)
            current_on_up_handle(event);
    }

    view.onMouseDown = function(event) {
        if (current_on_down_handle != null && !stop_all)
            current_on_down_handle(event);
    }

};
